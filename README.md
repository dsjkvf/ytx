ytx
===


## About

`ytx` is simple script aiming to be a RSS reader for YouTube. It parses the provided feeds stored in plain text in the `conf` file (see below), sorts uploads by date, and then presents them in Vim for future actions:

![](https://i.imgur.com/sXTcbCA.png)


## Configuration

For the configuration, `ytx` uses either the `$XDG_CONFIG_HOME/ytx/` folder, or just places the necessary files (`conf`, `last`, `hold`) alongside the script itself.


## Usage

In Vim, the following mappings are available

  - `gd` to download the current video or the visually selected videos
  - `gf` to filter videos by the current source / publisher
  - `gi` to view the information on the current video
  - `gh` to save the URL of the current video in the `hold` file
  - `gH` to manually edit the `hold` file
  - `gD` to download the videos saved in the `hold` file
  - `gp` to play the current video


## Dependencies:

`ytx` uses `curl` and `xidel` to parse the feeds, `sort`,`awk` and `Vim` to display the entries, `mpv` for the playback, and `yt-dlp` with `aria2c` for the downloads, so make sure to edit the download function manually if needed (see the helper function `ytd()`).

