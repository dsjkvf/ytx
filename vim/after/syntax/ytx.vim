" Syntax
syntax match  vidURL            /\thttp.*$/ conceal
syntax match  vidNew            /^• / conceal
syntax match  vidHeld           /^√ / conceal
syntax match  vidAuthorDate     /^\[.*\]/
syntax match  vidAuthorDateNew  /^• \[.*\]/ contains=vidNew
syntax match  vidAuthorDateHeld /^√ \[.*\]/ contains=vidHeld
syntax region vidTitle          start=/^\[.*\] / end=/\thttp.*$/ contains=vidAuthorDate,vidURL keepend
syntax region vidTitleNew       start=/^• \[.*\] / end=/\thttp.*$/ contains=vidNew,vidAuthorDateNew,vidURL keepend
syntax region vidTitleHeld      start=/^√ \[.*\] / end=/\thttp.*$/ contains=vidHeld,vidAuthorDateHeld,vidURL keepend

" Highlights
hi! def link vidAuthorDate     Comment
hi! def link vidAuthorDateNew  Comment
hi! def link vidAuthorDateHeld Comment
hi! def link vidtitle          String
hi! def link vidtitleNew       Question
hi! def link vidtitleHeld      SpecialKey

" Options
set conceallevel=3
set concealcursor=nv
