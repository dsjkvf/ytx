" Options
set showtabline=0
function! StatusLine()
    let msg = 'gi: Inspect    gf: Filter    gd: Download    gh: Hold    gH: Edit held    gD: Download held    gp: Play    q: Quit'
    let line = ''
    let line .= repeat(' ', (&columns - len(msg))/2->float2nr())
    let line .= msg
    return line
endfunction
set statusline=%#Question#%{StatusLine()}
set nolist
set cursorline
set matchpairs=

" Mappings
function! ytx#Selected(...)
    if a:000->len() > 0
        let f = a:1
    else
        let f = tempname()
    endif
    let z = getline("'<", "'>")->map('matchstr(v:val, "http.*")')
    for i in z
        call system('echo ' . i . ' >> ' . f)
    endfor
    if a:000->len() == 0
        noautocmd exe 'tab term ytx download-multi ' . f
    endif
endfunction
" select
nnoremap <buffer> <silent> t V
xnoremap <buffer> <silent> t j
" copy
nnoremap <buffer> <silent> yy :let @+ = getline('.')->matchstr('http.*')<CR>
" view details
nnoremap <buffer> <silent> gi :noautocmd let z = getline('.')->matchstr('http.*') \|
             \ noautocmd exe 'tab term ytx info ' . z<CR>
nmap <buffer> <silent> <CR> :let @+ = getline('.')->matchstr('https://[^ ]*')<CR>gi
" view filtered
nnoremap <buffer> <silent> gf :let c = matchstr(getline('.'), '.*\[\zs.*\ze\d\d\d\d.*\].*') \|
             \ let z = map(getline(1, '$'), "v:val =~ c ? v:val : ''")->filter('len(v:val) > 0') \|
             \ enew \|
             \ silent! put =z \|
             \ 1delete \|
             \ set ft=ytx<CR>
" play
nnoremap <buffer> <silent> gp :call system(['ytx', 'play', getline('.')->matchstr('http.*')]->join(' '))<CR>
" download
nnoremap <buffer> <silent> gd :let z = getline('.')->matchstr('http.*') \|
             \ noautocmd exe 'tab term ytx download ' . z<CR>
" download selected
xnoremap <buffer> <silent> gd :<C-u>call ytx#Selected()<CR>
" put on hold
nnoremap <buffer> <silent> gh :call system(['ytx', 'hold', getline('.')->matchstr('http.*')]->join(' '))<CR>
" put on hold selected
xnoremap <buffer> <silent> gh :<C-u>call ytx#Selected(system('ytx conf') . '/hold')<CR>
" edit held
nnoremap <buffer> <silent> gH :execute 'tabe ' . system('ytx conf') . '/hold'<CR>
" download held
nnoremap <buffer> <silent> gD :noautocmd exe 'tab term ytx download-held'<CR>
" quit
nnoremap <silent> <expr> q getbufinfo({'buflisted':1})->len() > 1
            \ ? ':bd<CR>:redraw!<CR>'
            \ : ':q!<CR>'
